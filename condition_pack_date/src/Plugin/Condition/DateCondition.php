<?php

namespace Drupal\condition_pack_date\Plugin\Condition;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a date-sensitive condition base.
 *
 * @Condition(
 *   id = "date",
 *   label = @Translation("Date, after"),
 * )
 */
class DateCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface, CacheableDependencyInterface {

  /**
   * The key for this plugin.
   *
   * @var string
   */
  protected $variable = 'date';

  /**
   * {@inheritdoc}
   */
  public function title() {
    return $this->t('Date, after');
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->t('The current date is after the input date.');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      $this->variable => '',
      'negate' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[$this->variable] = [
      '#type' => 'date',
      '#title' => $this->title(),
      '#default_value' => $this->configuration[$this->variable],
      '#description' => $this->description(),
      '#attached' => [
        'library' => [
          'condition_pack_date/drupal.condition_pack_date',
        ],
      ],
    ];
    $form = parent::buildConfigurationForm($form, $form_state);
    // This condition cannot be negated.
    $form['negate']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration[$this->variable] = $form_state->getValue($this->variable);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $value = $this->configuration[$this->variable];
    return $this->t('Shown on or after @value', ['@value' => implode(', ', $value)]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $value = $this->configuration[$this->variable];

    if (!$value && !$this->isNegated()) {
      return TRUE;
    }

    $value = (int) date('U', strtotime($value));
    $today = (int) date('U');

    // The DateBeforeCondition flag.
    if ($this->variable === 'date_before') {
      return $today < $value;
    }

    return $today >= $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Calculate the cache based on evaluation.
    $access = $this->evaluate();
    $before = $this->variable == 'date_before';
    $value = $this->configuration[$this->variable];
    $value = date('U', strtotime($value));
    $max_age = Cache::PERMANENT;

    // Have we passed the date? Cache forever.
    if ($access && !$before) {
      $max_age = Cache::PERMANENT;
    }
    // Not before but not accessible means we have not reached the date yet.
    elseif (!$access && !$before) {
      $max_age = $value - \Drupal::time()->getRequestTime();
    }
    // Before date but accessible means we have not reached the date yet.
    elseif ($access && $before) {
      $max_age = $value - \Drupal::time()->getRequestTime();
    }
    // Before but not accessible means the date has passed.
    elseif (!$access && $before) {
      $max_age = Cache::PERMANENT;
    }

    return $max_age;
  }

}
