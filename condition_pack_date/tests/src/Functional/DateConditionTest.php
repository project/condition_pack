<?php

namespace Drupal\Tests\condition_pack_date\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;

/**
 * Tests the date block visibility condition.
 *
 * @group condition_pack
 */
class DateConditionTest extends BrowserTestBase {

  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['condition_pack_date', 'block'];

  /**
   * Test domain navigation block.
   */
  public function testBlockVisibility() {
    $block_name = 'system_powered_by_block';
    $block = $this->placeBlock($block_name);

    $text = "Powered by Drupal";

    // Confirm that the block is displayed on the front page.
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);

    // Only show after a past date.
    $block->delete();
    $settings = [
      'visibility' => [
        'date' => [
          'id' => 'date',
          'date' => '2020-12-31',
          'negate' => FALSE,
        ],
      ],
    ];
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);

    // Now let's negate (reverse) the condition.
    $block->delete();
    $settings['visibility']['date']['negate'] = TRUE;
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains($text);

    // Now try with a date set in the future.
    $block->delete();
    $settings = [
      'visibility' => [
        'date' => [
          'id' => 'date',
          'date' => '2030-12-31',
          'negate' => FALSE,
        ],
      ],
    ];
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains($text);

    // Now let's negate (reverse) the condition.
    $block->delete();
    $settings['visibility']['date']['negate'] = TRUE;
    $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);
  }

}
