<?php

namespace Drupal\Tests\condition_pack_date\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;

/**
 * Tests the before date block visibility condition.
 *
 * @group condition_pack
 */
class DateBeforeConditionTest extends BrowserTestBase {

  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['condition_pack_date', 'block'];

  /**
   * Test domain navigation block.
   */
  public function testBlockVisibility() {
    $block_name = 'system_powered_by_block';
    $block = $this->placeBlock($block_name);

    $text = "Powered by Drupal";

    // Confirm that the block is displayed on the front page.
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);

    // Only show before a future date.
    $block->delete();
    $settings = [
      'visibility' => [
        'date_before' => [
          'id' => 'date_before',
          'date_before' => '2030-12-31',
          'negate' => FALSE,
        ],
      ],
    ];
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);

    // Now let's negate (reverse) the condition.
    $block->delete();
    $settings['visibility']['date_before']['negate'] = TRUE;
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains($text);

    // Now try with before set in the past.
    $block->delete();
    $settings = [
      'visibility' => [
        'date_before' => [
          'id' => 'date_before',
          'date_before' => '2020-12-31',
          'negate' => FALSE,
        ],
      ],
    ];
    $block = $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains($text);

    // Now let's negate (reverse) the condition.
    $block->delete();
    $settings['visibility']['date_before']['negate'] = TRUE;
    $this->placeBlock($block_name, $settings);
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($text);
  }

}
